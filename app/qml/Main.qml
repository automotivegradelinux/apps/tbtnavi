import QtQuick
import QtQuick.Controls

import QtQuick.Window

ApplicationWindow {
    id: tbtnavi

    title: "Turn By Turn Navigation Demo"
    height: Window.height
    width: Window.width
    flags: Qt.FramelessWindowHint
    visible: true

    property double vehicleSpeed: 0
    property double engineSpeed: 0
    property bool mphDisplay: false

    Component.onCompleted : {
        VehicleSignals.connect()
    }

    Connections {
        target: VehicleSignals

        onConnected: {
	    VehicleSignals.authorize()
        }

        onAuthorized: {
	    VehicleSignals.subscribe("Vehicle.Speed")
	    VehicleSignals.subscribe("Vehicle.Powertrain.CombustionEngine.Speed")
	    VehicleSignals.subscribe("Vehicle.Cabin.SteeringWheel.Switches.Info")
	    VehicleSignals.get("Vehicle.Cabin.Infotainment.HMI.DistanceUnit")
	    VehicleSignals.subscribe("Vehicle.Cabin.Infotainment.HMI.DistanceUnit")
	}

        onGetSuccessResponse: {
            //console.log("response path = " + path + ", value = " + value)
            if (path === "Vehicle.Cabin.Infotainment.HMI.DistanceUnit") {
                if (value === "km") {
                    mphDisplay = false
                } else if (value === "mi") {
                    mphDisplay = true
                }
            }
        }

        onSignalNotification: {
            //console.log("signal path = " + path + ", value = " + value)
            if (path === "Vehicle.Speed") {
                // value units are always km/h
                if (mphDisplay)
                    vehicleSpeed = parseFloat(value) * 0.621504
                else
                    vehicleSpeed = parseFloat(value)
            } else if (path === "Vehicle.Powertrain.CombustionEngine.Speed") {
                engineSpeed = parseFloat(value)
            } else if (path === "Vehicle.Cabin.SteeringWheel.Switches.Info" && value === "true") {
                console.log ("Switching views!")
                console.log ("currentIndex = " + container.currentIndex)
                if (container.currentIndex === 0) {
                    console.log ("Switching to info")
                    container.currentIndex = 1
                } else if (container.currentIndex === 1) {
                    console.log ("Switching to map")
                    container.currentIndex = 0
                }
            } else if (path === "Vehicle.Cabin.Infotainment.HMI.DistanceUnit") {
                if (value === "km") {
                    mphDisplay = false
                } else if (value === "mi") {
                    mphDisplay = true
                }
            }
        }
    }

    Item {
        id: container
        anchors.centerIn: parent
        width: parent.width
        height: parent.height

        property int currentIndex: 0

        MapWindow {
            visible: parent.currentIndex === 0
            opacity: parent.currentIndex === 0 ? 1.0 : 0.0
            anchors.fill: parent
            objectName: "mapwindow"
        }

        InfoWindow {
            visible: parent.currentIndex === 1
            opacity: parent.currentIndex === 1 ? 1.0 : 0.0
            anchors.fill: parent
        }
    }
}
