﻿import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    id: infoWindow

    anchors.fill: parent
    color: "black"

    property date now: new Date
    Timer {
        interval: 100; running: true; repeat: true;
        onTriggered: parent.now = new Date
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Text {
            Layout.fillWidth: true
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: parent.height / 10

            text: now.toLocaleString(Qt.locale("en_US"), 'h:mm A')
            font.pixelSize: 40
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            padding: 0
        }

        Text {
            id: speedText
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: parent.height / 10

            text: tbtnavi.vehicleSpeed.toFixed(0)
            color: "white"
            font.pixelSize: 280
            // Slight hack to shrink the border around the text by tweaking
            // the line height, note that it is font size dependent
            lineHeight: 0.8
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            padding: 0
        }
        Text {
            id: speedLabel
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter

            text: tbtnavi.mphDisplay ? "MPH" : "KPH"
            color: "white"
            font.pixelSize: 32
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            padding: 0
        }

        Text {
            id: rpmText
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: parent.height / 16

            text: tbtnavi.engineSpeed.toFixed(0)
            color: "white"
            font.pixelSize: 64
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            padding: 0
        }
        Text {
            id: rpmLabel
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter

            text: "RPM"
            color: "white"
            font.pixelSize: 32
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            padding: 0
        }

        // Use height filling rectangle to push things together,
        // saving tinkering with minimum/preferred heights
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "black"
        }
    }
}
