#ifndef NAVIGATION_CLIENT_H
#define NAVIGATION_CLIENT_H

#include <navigation.h>
#include <QtQml/QQmlApplicationEngine>

class navigation_client : public QObject {
    Q_OBJECT

    Navigation *m_navigation;

public:
    navigation_client(Navigation *navigation, QObject *parent = nullptr);
    ~navigation_client();

signals:
    //notify add routepoints signal to qml
    void addRoutePointsQml(QVariant, QVariant, QVariant, QVariant);
    //notify current position signal to qml
    void positionQml(QVariant, QVariant,QVariant, QVariant);
    //notify stop demo signal to qml
    void stopdemoQml();
    //notify arrive destination signal to qml
    void arrivedestQml();

private slots:
    void onStatusEvent(QVariantMap data);
    void onPositionEvent(QVariantMap data);
};
#endif // NAVIGATION_CLIENT_H
