#include <QQmlApplicationEngine>

#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickWindow>
#include <navigation.h>
#include <vehiclesignals.h>
#include <glib.h>

#include "navigation_client.h"
#include "qcheapruler.hpp"
#include "file_operation.h"

#include <cstdlib>
#include "AglShellGrpcClient.h"

std::string
read_config_and_get_output(void)
{
	char *ret;

	ret = getenv("OUTPUT_NAME");
	if (!ret)
		return std::string("");

	return std::string(ret);
}

int main(int argc, char *argv[])
{
	std::string our_name = "tbtnavi";
	QString graphic_role = QString(our_name.c_str());

	setenv("QT_QUICK_CONTROLS_STYLE", "AGL", 1);

	QGuiApplication app(argc, argv);
	QCoreApplication::setOrganizationDomain("automotivelinux.org");
	QCoreApplication::setOrganizationName("AutomotiveGradeLinux");
	QCoreApplication::setApplicationName(graphic_role);
	QCoreApplication::setApplicationVersion("0.1.0");
	app.setDesktopFileName(graphic_role);

	// grab the output to put the application on
	std::string output = read_config_and_get_output();

	GrpcClient *client = new GrpcClient();
	client->WaitForConnected(500, 10);
	if (!output.empty())
		client->SetAppOnOutput(our_name, output);
	else
		client->SetAppOnOutput(our_name, "pipewire");

	// Load qml
	QQmlApplicationEngine engine;

	qmlRegisterType<QCheapRuler>("com.mapbox.cheap_ruler", 1, 0, "CheapRuler");

	QQmlContext *context = engine.rootContext();

	File_Operation file;
	context->setContextProperty("fileOperation", &file);

	VehicleSignalsConfig vsConfig("tbtnavi");
	VehicleSignals *vs = new VehicleSignals(vsConfig);
	if (!vs) {
		qFatal("Could not create VehicleSignals!");
	}
	context->setContextProperty("VehicleSignals", vs);

	// Give the navigation client it's own vehicle signals connection
	// to simplify state management wrt QML initialization, and keep the
	// notification streams separate.
	Navigation *navigation = new Navigation(new VehicleSignals(vsConfig), false, context);

	engine.load(QUrl(QStringLiteral("qrc:/Main.qml")));

	QObject *root = engine.rootObjects().first();
	new navigation_client(navigation, root->findChild<QObject*>("mapwindow"));

	return app.exec();
}
