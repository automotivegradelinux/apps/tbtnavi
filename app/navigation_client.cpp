#include <QDebug>
#include "navigation_client.h"

navigation_client::navigation_client(Navigation *navigation, QObject *parent) :
  m_navigation(navigation)
{
    QObject::connect(m_navigation, &Navigation::statusEvent, this, &navigation_client::onStatusEvent);
    QObject::connect(m_navigation, &Navigation::positionEvent, this, &navigation_client::onPositionEvent);

    // connect the signal to qml inside function(addRoutePointsQml -> do_addRoutePoint)
    QObject::connect(this, SIGNAL(addRoutePointsQml(QVariant, QVariant, QVariant, QVariant)),
                         parent, SLOT(do_addRoutePoint(QVariant, QVariant, QVariant, QVariant)));

    // connect the signal to qml inside function(positionQml -> do_setCoordinate)
    QObject::connect(this, SIGNAL(positionQml(QVariant, QVariant,QVariant, QVariant)),
                         parent, SLOT(do_setCoordinate(QVariant, QVariant,QVariant, QVariant)));

    // connect the signal to qml inside function(stopdemoQml -> do_stopnavidemo)
    QObject::connect(this, SIGNAL(stopdemoQml()), parent, SLOT(do_stopnavidemo()));

    // connect the signal to qml inside function(arrivedestQml -> do_arrivedest)
    QObject::connect(this, SIGNAL(arrivedestQml()), parent, SLOT(do_arrivedest()));
}

navigation_client::~navigation_client(){}

void navigation_client::onStatusEvent(QVariantMap data)
{
    if (!data.contains("state"))
        return;

    QString state = data["state"].toString();

    if (state == "ARRIVED")
        emit arrivedestQml();

    if (state == "STOPPED")
        emit stopdemoQml();
}

void navigation_client::onPositionEvent(QVariantMap data)
{
    if (!data.contains("position"))
        return;;

    QString position = data["position"].toString();

    if (position == "route") {
        double route_Lat_s = data["latitude"].toDouble();
        double route_Lon_s = data["longitude"].toDouble();
        double route_Lat_e = data["route_latitude"].toDouble();
        double route_Lon_e = data["route_longitude"].toDouble();

        emit addRoutePointsQml(route_Lat_s, route_Lon_s, route_Lat_e, route_Lon_e);
    }

    if (position == "car") {
        double cur_Lat_p = data["latitude"].toDouble();
        double cur_Lon_p = data["longitude"].toDouble();
        double cur_direction = data["direction"].toDouble();
        double cur_distance = data["distance"].toDouble();

        emit positionQml(cur_Lat_p, cur_Lon_p, cur_direction, cur_distance);
    }
}
