﻿# tbtnavi
Navigation application with turn by turn (TBT) navigation support for cluster demo remote display.
## Reference
The application is based on the QtLocation widget, documentation is available at:
https://doc-snapshots.qt.io/qt5-5.9/qtlocation-index.html
and example code used as a starting point at:
https://doc-snapshots.qt.io/qt5-5.9/qtlocation-mapviewer-example.html

## Build
```
source ${SDK_PATH}/environment-setup-aarch64-agl-linux
mkdir build
cd build
qmake ../
make
```
## Configuration

This is the same as the configuration file for the ondemandnavi navigation application.  The same file will be used by both applications if present.

Please set mapAccessToken, mapStyleUrl, speed, interval, latitude and longitude in JSON format in `/etc/naviconfig.ini`.

- mapAccessToken sets Access token provided by Mapbox.
- speed sets vehicle speed in km/h.
- interval sets the screen update interval in milliseconds.
- latitude, longitude sets the current position at application start.
- mapStyleUrls sets Mapbox style URLs.

### Example /etc/naviconfig.ini
```
example
{
	"mapAccessToken":"pk.***********",
	"speed":60,
	"interval":100,
	"latitude":36.1363,
	"longitude":-115.151,
	"mapStyleUrls":"mapbox://styles/mapbox/dark-v9"
}
```
## Licensing
All files are Apache 2.0 licensed, except for those under include/mapbox.
### From https://github.com/mapbox/cheap-ruler-cpp
- include/mapbox/cheap_ruler.hpp

ISC license (see LICENSE.mapbox-cheap-ruler-cpp)

### From https://github.com/mapbox/geometry.hpp
- feature.hpp
- geometry.hpp
- geometry_io.hpp
- geometry/box.hpp
- geometry/empty.hpp
- geometry/envelope.hpp
- geometry/for_each_point.hpp
- geometry/geometry.hpp
- geometry/line_string.hpp
- geometry/multi_line_string.hpp
- geometry/multi_point.hpp
- geometry/multi_polygon.hpp
- geometry/point_arithmetic.hpp
- geometry/point.hpp
- geometry/polygon.hpp

ISC license (see LICENSE.mapbox-geometry.hpp)

### From https://github.com/mapbox/variant
- optional.hpp
- variant_cast.hpp
- variant.hpp
- variant_io.hpp
- variant_visitor.hpp

BSD 3 Clause license (see LICENSE.mapbox-variant)

- recursive_wrapper.hpp

Boost 1.0 license per header comment (see LICENSE_1.0.txt)

